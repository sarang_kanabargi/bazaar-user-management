[![Codacy Badge](https://api.codacy.com/project/badge/Grade/1528f3c339b34df5af0e89adfcfc6ac6)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=sarang_kanabargi/bazaar-user-management&amp;utm_campaign=Badge_Grade)

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### User Management API for bazaar ###

* User Management API for bazaar.io. API Support, GET, POST and PUT operations for Registered User's of bazaar.io
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### API Setup ###

* gradlew clean build
* Configuration
* Dependencies
* Database configuration
* How to run tests
* gradlew bootRun

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* User Management Team @ bazaar.io 
* user.management@bazaar.io